// const express = require('express');
// const bodyParser = require('body-parser');
// const path = require('path');
// const http = require('http');
// const app = express();
import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';
import logger from 'morgan';
import mongoose from 'mongoose';
import SourceMapSupport from 'source-map-support';
// API file for interacting with MongoDB
import routes from './server/routes/api';
const app = express();

// allow-cors
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET, POST, DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})
// Parsers
app.use(bodyParser.json());
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true}));
// Angular DIST output folder
app.use(express.static(path.join(__dirname, 'dist')));

//Set Port
const port = process.env.PORT || '3000';
// connect to database
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/logs', {
});
// add Source Map Support
SourceMapSupport.install()
// API location
app.use('/api', routes);
app.get('/', (req,res) => {
    return res.end('Api working');
  })
  // catch 404
app.use((req, res, next) => {
    res.status(404).send('<h2 align=center>Page Not Found!</h2>');
  });
  // start the server
  app.listen(port,() => {
    console.log(`App Server Listening at ${port}`);
  });

// // Send all other requests to the Angular app
// app.get('*', (req, res) => {
//     res.sendFile(path.join(__dirname, 'dist/index.html'));
// });


// app.set('port', port);

// const server = http.createServer(app);

// server.listen(port, () => console.log(`Running on localhost:${port}`));