import mongoose from 'mongoose';

//import models
import Task from './task.server.model';

export const getTasks = (req,res) => {
    Task.find().sort({name: 1}).exec((err,tasks) => {
      if(err){
      return res.json({'success':false,'message':'Some Error'});
      }
      return res.json({'success':true,'message':'Logs fetched successfully',tasks});
  });
}
export const addTask = (req,res) => {
  const newTask = new Task(req.body);
  console.log(newTask);
  newTask.save((err,task) => {
    if(err){
    return res.json({'success':false,'message':'Some Error'});
    }
return res.json({'success':true,'message':'Task added successfully', task});
  })
}