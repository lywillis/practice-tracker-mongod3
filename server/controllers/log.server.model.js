import mongoose from 'mongoose';

var logSchema = mongoose.Schema({
    start: Date, 
    end: Date,
    task: String
})
export default mongoose.model('Log', logSchema);