import mongoose from 'mongoose';

var taskSchema = mongoose.Schema({
    name: String
})
export default mongoose.model('Task', taskSchema);