import mongoose from 'mongoose';

//import models
import Log from './log.server.model';

export const getLogs = (req,res) => {
  if(req.query.start && req.query.end)
  {
    return getLogsForTimeRange (req,res);
  }
    Log.find().sort({start: 1}).exec((err,logs) => {
      if(err){
      return res.json({'success':false,'message':'Some Error'});
      }
      return res.json({'success':true,'message':'Logs fetched successfully',logs});
  });
}

export const getLogsForTimeRange = (req, res) => {
  Log.find(
    {
      start: { $gte: req.query.start },
      end: {
        $lte: req.query.end
      }
    }

  ).sort({start: 1}).exec((err,logs) => {
    if(err){
    return res.json({'success':false,'message':'Some Error'});
    }
    return res.json({'success':true,'message':'Logs fetched successfully',logs});
});
}
export const addLog = (req,res) => {
  const newLog = new Log(req.body);
  newLog.save((err,log) => {
    if(err){
    return res.json({'success':false,'message':'Some Error'});
    }
return res.json({'success':true,'message':'Log added successfully', log});
  })
}

export const getLog = (req,res) => {
  Log.find({_id:req.params.id}).exec((err,log) => {
    if(err){
    return res.json({'success':false,'message':'Some Error'});
    }
    if(log.length){
      return res.json({'success':true,'message':'Log fetched by id successfully',log});
    }
    else{
      return res.json({'success':false,'message':'Log with the given id not found'});
    }
  })
}

export const deleteLog = (req, res) => {
  Log.findByIdAndRemove(req.params.id, (err, log) => {
    if (err) {
      return res.json({ 'success': false, 'message': 'Some Error' });
    }
    return res.json({ 'success': true, 'message': log._id + 'deleted successfully' });
  })
}