// const express = require('express');
// const router = express.Router();
// const MongoClient = require('mongodb').MongoClient;
// const ObjectID = require('mongodb').ObjectID;
// const d3 = require('d3');
import express from 'express';
import mongoose from 'mongoose';
//import controller file
import * as logController from '../controllers/log.server.controller';
import * as taskController from '../controllers/task.server.controller';

// get an instance of express router
const router = express.Router();
router.route('/logs')
     .get(logController.getLogs)
     .post(logController.addLog);

router.route('/log/:id')
    .get(logController.getLog)
    .delete(logController.deleteLog);

router.route('/tasks/')
    .get(taskController.getTasks)
    .post(taskController.addTask);

export default router;

// // Connect
// const connection = (closure) => {
//     return MongoClient.connect('mongodb://localhost:27017/logs', (err, client) => {
//         if (err) return console.log(err);
//         let db = client.db('logs');
//         closure(db);
//     });
// };

// // Error handling
// const sendError = (err, res) => {
//     response.status = 501;
//     response.message = typeof err == 'object' ? err.message : err;
//     res.status(501).json(response);
// };

// // Response handling
// let response = {
//     status: 200,
//     data: [],
//     message: null
// };

// // Get tasks
// router.get('/tasks', (req, res) => {
//     connection((db) => {
//         db.collection('tasks')
//             .find()
//             .toArray()
//             .then((tasks) => {
//                 response.data = tasks;
//                 res.json(response);
//             })
//             .catch((err) => {
//                 sendError(err, res);
//             });
//     });
// });
// router.get('/logs', (req, res) => {
//     connection((db) => {
//         db.collection('logs')
//             .find()
//             .sort({start: 1})
//             .limit(15)
//             .toArray()
//             .then((logs) => {
//                 response.data = logs;
//                 res.json(response);
//             })
//             .catch((err) => {
//                 sendError(err, res);
//             });
//     });
// });

// // router.get('/logs/:task', (req, res) => {
// //     connection((db) => {
// //         db.collection('logs').find({
// //             task: new ObjectID(req.params.task)
// //         }).sort({start: 1})
// //             .toArray()
// //             .then((logs) => {
// //                 response.data = logs;
// //                 res.json(response);
// //             })
// //             .catch((err) => {
// //                 sendError(err, res);
// //             });
// //     });
// // });

// router.get('/logs/:task', (req, res) => {
//     connection((db) => {
//         db.collection('logs').find({
//             start: {
//                 $gte: d3.isoParse(req.query.start),
//                 $lte: d3.isoParse(req.query.end)
//             },
//             task: new ObjectID(req.params.task)
            
//         }).sort({start: 1})
//             .toArray()
//             .then((logs) => {
//                 response.data = logs;
//                 res.json(response);
//             })
//             .catch((err) => {
//                 sendError(err, res);
//             });
//     });
// });
// router.get('/addLog', (req, res) => {
//     connection((db) => {
//         db.collection('logs').insertOne
//             ({
//                 start: d3.isoParse(req.query.start),
//                 end: d3.isoParse(req.query.end),
//                 task: req.query.task
//             }
//             )
//             .then(log => {
                
//                 response.data = log;
//                res.json(response);
               
//             })
//             .catch((err) => {
//                 sendError(err, res);
//             });

// });
// }
// )
// router.get('/addTask/:task', (req, res) => {
//     connection((db) => {
//         db.collection('tasks').insertOne
//             ({
//                 name: req.params.task
//             }
//             )
//             .then(task => {
                
//                 response.data = task;
//                res.json(response);
               
//             })
//             .catch((err) => {
//                 sendError(err, res);
//             });

// });
// }
// )
// router.delete('/removeTask/:id', (req, res) => {
//     connection((db) => {
//         db.collection('tasks').deleteOne
//             ({
//                 _id: new ObjectID(req.params.id)
//             }
//             )
//             .then(task => {
                
//                 response.data = task;
//                res.json(response);
               
//             })
//             .catch((err) => {
//                 sendError(err, res);
//             });

// });
// }
// )


module.exports = router;