import { Component, OnInit } from '@angular/core';
import { MongoDBService } from '../../services/mongoDB.service';
import { Subscription } from 'rxjs';
import { DataButtonService } from '../../services/data-button.service';
import { TimeLog} from '../../Models/LogFormat';
import * as d3 from 'd3';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddLogComponent } from '../add-log/add-log.component';

export interface FormattedLog {
  startDate: string;
  startTime: string;
  duration: number;
  task: string;
  _id: string;
}

@Component({
  selector: 'app-log-list',
  templateUrl: './log-list.component.html',
  styleUrls: ['./log-list.component.less']
})
export class LogListComponent implements OnInit {
  logs: Array<TimeLog>;
  formattedLogs: Array<FormattedLog>;
  dataSpecSub: Subscription;
  mongoSub: Subscription;
  startTime: Date;
  endTime: Date;
  task: string;
  selectedLog: FormattedLog;
  constructor(private mongoDBService: MongoDBService, private dataButtonService: DataButtonService, private modalService: NgbModal) { }
  ngOnInit() {
    this.getLogs();
    this.mongoDBService.logAddSubject.subscribe(() => this.getLogs());
    // this.dataSpecSub = this.dataButtonService.dataQueryParms$.subscribe(status => {
    //   this.startTime = status.startTime;
    //   this.endTime = status.endTime;
    //   this.task = status.task;
    //   this.mongoSub = this.mongoDBService.getLogs(status.task, status.startTime, status.endTime).subscribe((logs: Array<TimeLog>) => {
    //     this.logs = logs;
    //     this.formattedLogs = this.formatLogs(logs);
    //   });
    // });
  }
  getLogs() {
    this.mongoDBService.getAllLogs().then(res => {
      this.logs = res;
      this.formattedLogs = this.formatLogs(res);
    });
  }
  formatLogs(logs: Array<TimeLog>) {
    return logs.map(log => {
      const start = d3.isoParse(log.start);
      const end = d3.isoParse(log.end);
      return {
        startDate: d3.timeFormat('%b %d')(start),
        startTime: d3.timeFormat('%X')(start),
        duration: d3.timeMinute.count(start, end),
        task: log.task,
        _id: log._id
      };
    });
  }
  onSelect(log: FormattedLog) {
    this.selectedLog = log;
  }

  openAddLog() {
    this.modalService.open(AddLogComponent, {centered: false, size: 'sm'});
  }
  deleteLog(log: FormattedLog) {
    this.mongoDBService.deleteLog(log._id);
  }
}
