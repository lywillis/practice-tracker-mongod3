import { Component, OnInit } from '@angular/core';
import { MongoDBService } from '../../services/mongoDB.service';
import { Task, TimeLog } from '../../Models/LogFormat';
import {NgbActiveModal, NgbDropdown} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-log',
  templateUrl: './add-log.component.html',
  styleUrls: ['./add-log.component.css']
})
export class AddLogComponent implements OnInit {

  constructor(private mongoDBService: MongoDBService, public activeModal: NgbActiveModal) { }
  tasks: Array<Task>;
  currentLog: any = {};
  elapsedTime: any;
  ngOnInit() {
    this.mongoDBService.getTasks().then(res => {
      this.tasks = res;
      this.currentLog.task = this.tasks[0].name;
    });
  }

  completeLog(form: any) {
    this.mongoDBService.addLog(this.currentLog);
    this.currentLog = {};
    this.elapsedTime = undefined;
    this.activeModal.close();
  }
  enterTime(timeEvent: any) {
    this.currentLog.start = timeEvent.start;
    this.currentLog.end = timeEvent.end;
  }
  displayElapsedTime(event: any) {
    this.elapsedTime = event;
  }
}
