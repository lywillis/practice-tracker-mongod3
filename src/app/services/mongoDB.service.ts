import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import {map, tap } from 'rxjs/operators';
import * as d3 from 'd3';
import { Task, TimeLog} from '../Models/LogFormat';
import { Subject, Observable } from 'rxjs';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class MongoDBService {
  private apiUrl = 'http://localhost:3000/api/';
  taskAddSubject: Subject<Task> = new Subject();
  logAddSubject: Subject<TimeLog> = new Subject();
  constructor(private http: Http) {
   }

  getAllLogs(): Promise<Array<TimeLog>> {
    return this.http.get(this.apiUrl + 'logs/').toPromise().then(res => {
      return this.handleData(res).logs;
    }).catch(this.handleError);

}
getTasks(): Promise<Array<Task>> {
  return this.http.get(this.apiUrl + 'tasks/').toPromise().then(res => {
    return this.handleData(res).tasks;
  }).catch(this.handleError);

}
deleteLog(id: string) {
  return this.http.delete(this.apiUrl + 'log/' + id).toPromise().then(res => {
    this.handleData(res);
    this.logAddSubject.next();
  });
}

getLogs(start: Date, end: Date) {
    return this.http.get(`${this.apiUrl}logs?start=${start}&end=${end}`).toPromise().then(res => {
      return this.handleData(res).logs;
    }).catch(this.handleError);
}
  // getLogs(task: Task, start?: Date, end?: Date) {
  //   const startStr = d3.isoFormat(start);
  //   const endStr = d3.isoFormat(end);
  //   const taskID = task.name;
  //   return this.http.get(`/api/logs/${taskID}?start=${startStr}&end=${endStr}`).pipe(map(result => {
  //     return result.json().data;
  //   }



addLog(log: TimeLog) {
    return this.http.post(this.apiUrl + 'logs/', log).toPromise().then(res => {
      this.handleData(res);
      this.logAddSubject.next();
    }
      ).catch(this.handleError);
}

addTask(task: Task) {
  return this.http.post(this.apiUrl + 'tasks/', task).toPromise().then(res => {
    this.handleData(res);
    this.taskAddSubject.next();
    }).catch(this.handleError);
}

private handleData(res: any) {
  const body = res.json();
  console.log(body); // for development purposes only
  return body || {};
}
private handleError(error: any): Promise<any> {
console.error('An error occurred', error); // for development purposes only
return Promise.reject(error.message || error);
}
}
