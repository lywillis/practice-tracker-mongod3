import { Injectable, EventEmitter } from '@angular/core';
import { TimeCategoryEnum } from '../Models/TimeCategoryEnum';
import * as d3 from 'd3';
import { Task } from '../Models/LogFormat';

@Injectable({
  providedIn: 'root'
})
export class DataButtonService {
  dataQueryParms$: EventEmitter<any> = new EventEmitter();
  timeCategory: TimeCategoryEnum;
  endTime: Date;
  startTime: Date;
  task?: Task;
  constructor() { }

  setTimeCategory(timeCategory: TimeCategoryEnum) {
    if (timeCategory !== this.timeCategory) {
    this.timeCategory = timeCategory;
    this.startTime = this.getRelativeStartTime();
    this.endTime = this.getRelativeEndTime(this.startTime);
    }
    this.dataQueryParms$.emit({
      timeCategory: this.timeCategory,
      startTime: this.startTime,
      endTime: this.endTime,
      task: this.task
    });
  }
  private getRelativeStartTime() { // start time relative to now
    const now = new Date();
    switch (this.timeCategory) {
      case TimeCategoryEnum.Month: {
        // beginning of month
        return d3.timeMonth(now);
      }
      case TimeCategoryEnum.Year: {
        // beginning of year
        return d3.timeYear(now);
      }
      case TimeCategoryEnum.Day: {
        return d3.timeDay(now);
      }
      case TimeCategoryEnum.Week: { // first day of week is sunday
        return d3.timeWeek(now);
      }

    }
  }
  getRelativeEndTime(startTime: Date) {
    switch (this.timeCategory) {
      case TimeCategoryEnum.Month: {
        // end of this month
        return d3.timeMonth.offset(startTime, 1);
      }
      case TimeCategoryEnum.Year: {
        return d3.timeYear.offset(startTime, 1);
      }
      case TimeCategoryEnum.Day: {
        return d3.timeDay.offset(startTime, 1);
      }
      case TimeCategoryEnum.Week: {
        return d3.timeWeek.offset(startTime, 1);
      }
    }
  }

  setTask(task: Task) {
    this.task = task;
    if (this.startTime && this.endTime) {
    this.dataQueryParms$.emit({
      timeCategory: this.timeCategory,
      startTime: this.startTime,
      endTime: this.endTime,
      task: this.task
    });
  }
}

subtractOne(timeCategory: TimeCategoryEnum, startTime: Date) {
  switch (timeCategory) {
    case TimeCategoryEnum.Month: {
      return d3.timeMonth.offset(startTime, -1);
    }
    case TimeCategoryEnum.Year: {
      return d3.timeYear.offset(startTime, -1);
    }
    case TimeCategoryEnum.Day: {
      return d3.timeDay.offset(startTime, -1);
    }
    case TimeCategoryEnum.Week: {
      return d3.timeWeek.offset(startTime, -1);
    }
  }
}
goBack() {
  this.startTime = this.subtractOne(this.timeCategory, this.startTime);
  this.endTime = this.getRelativeEndTime(this.startTime);
  this.dataQueryParms$.emit({
    timeCategory: this.timeCategory,
    startTime: this.startTime,
    endTime: this.endTime,
    task: this.task
  });
}
}

