import { TestBed, inject } from '@angular/core/testing';

import { DataButtonService } from './data-button.service';

describe('DataButtonService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DataButtonService]
    });
  });

  it('should be created', inject([DataButtonService], (service: DataButtonService) => {
    expect(service).toBeTruthy();
  }));
});
