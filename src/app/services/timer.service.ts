import { Injectable, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TimerService {
  private play = false;
  private pause = false;
  private stop = true;
  private startTime = 0;
  public timerStatus$ = new EventEmitter();
  constructor() { }

  startTimer() {
    this.play = true;
    this.pause = false;
    this.stop = false;
    this.timerStatus$.emit({
      play: this.play
    });
  }
  pauseTimer() {
    this.play = false;
    this.pause = true;
    this.stop = false;
    this.timerStatus$.emit({
      pause: this.pause
    });
  }
  stopTimer() {
    this.play = false;
    this.pause = false;
    this.stop = true;
    this.timerStatus$.emit({
      stop: this.stop
    });
  }
}
