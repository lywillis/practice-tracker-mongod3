import { TestBed, inject } from '@angular/core/testing';

import { MongoDBService } from './mongoDB.service';

describe('LogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MongoDBService]
    });
  });

  it('should be created', inject([MongoDBService], (service: MongoDBService) => {
    expect(service).toBeTruthy();
  }));
});
