import { Component, ViewEncapsulation } from '@angular/core';
import { AddLogComponent } from './log/add-log/add-log.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  constructor() {
}

}
