import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DataButtonService } from '../../services/data-button.service';
import { MongoDBService } from '../../services/mongoDB.service';
import { Subscription } from 'rxjs';
import { Task } from '../../Models/LogFormat';

@Component({
  selector: 'app-task-select',
  templateUrl: './task-select.component.html',
  styleUrls: ['./task-select.component.css']
})
export class TaskSelectComponent implements OnInit {
  tasks: Array<Task>;
  @Output() selectedTask$ = new EventEmitter<Task>();
  selectedTask: Task;
  mongoSub: Subscription;
  constructor(private dataButtonService: DataButtonService, private mongoDBService: MongoDBService) { }

  ngOnInit() {
    this.mongoDBService.getTasks().then(tasks => {
      this.tasks = tasks;
      this.selectedTask = tasks[0];
      this.onTaskChange(this.selectedTask);
      // this.dataButtonService.setTask(this.selectedTask);
    });
  }
  onTaskChange(task: Task) {
    this.selectedTask$.emit(this.selectedTask);
  }

}
