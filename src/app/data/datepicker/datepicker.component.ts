import { Component, OnInit } from '@angular/core';
import { NgbDateStruct, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';

const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.less']
})
export class DatepickerComponent implements OnInit {
  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;

  constructor(calendar: NgbCalendar) { }

  ngOnInit() {
  }

  onDateSelection(date: NgbDateStruct) {
    if (!this.fromDate && !this.toDate) { // neither dates set
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && after(date, this.fromDate)) { // start date already set
      this.toDate = date;
    } else if (this.fromDate && before(date, this.fromDate)) { // end behind start date, so becomes new start date
      this.toDate = this.fromDate;
      this.fromDate = date;
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  isInside = date => after(date, this.fromDate) && before(date, this.toDate);
  isFrom = date => equals(date, this.fromDate);
  isTo = date => equals(date, this.toDate);
}
