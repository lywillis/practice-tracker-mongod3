import { Component, OnInit, MissingTranslationStrategy } from '@angular/core';
import { MongoDBService } from '../../services/mongoDB.service';
import { TimeLog, Task } from '../../Models/LogFormat';
import * as d3 from 'd3';
import { TimeCategoryEnum } from '../../Models/TimeCategoryEnum';
import { DataButtonService } from '../../services/data-button.service';
import { Subscription } from 'rxjs';
import * as moment from 'moment';
import { ElementSchemaRegistry } from '@angular/compiler';

export interface AggregationData {
  date: Date; // date bucket it falls into
  timeLogged: number;
}
@Component({
  selector: 'app-log-data',
  templateUrl: './log-data.component.html',
  styleUrls: ['./log-data.component.less']
})
export class LogDataComponent implements OnInit {
  timeCategory: TimeCategoryEnum = TimeCategoryEnum.Month; // default
  data: Array<AggregationData>;
  startTime: Date;
  endTime: Date;
  task: Task;
  // subs
  dataSpecSub: Subscription;
  mongoSub: Subscription;
  // time category options
  options?: string[] = Object.keys(TimeCategoryEnum).filter(key => isNaN(Number(key)));
  constructor(private mongoDBService: MongoDBService,
  private dataButtonService: DataButtonService) { }

  ngOnInit() {
    // this.dataButtonService.setTimeCategory(TimeCategoryEnum.Month);
    this.dataSpecSub = this.dataButtonService.dataQueryParms$.subscribe(status => {
      this.startTime = status.startTime;
      this.endTime = status.endTime;
      this.timeCategory = status.timeCategory;
      this.task = status.task;
      this.mongoDBService.getAllLogs().then((logs: Array<TimeLog>) => {
        this.aggregateData(logs);
      });
    });
  }

  aggregateData(logs: TimeLog[]) {
    switch (this.timeCategory) {
      case TimeCategoryEnum.Year: {
        this.data = this.aggregateByMonth(logs);
        break;
      }
      case TimeCategoryEnum.Month:
      case TimeCategoryEnum.Week: {
        this.data = this.aggregateByDay(logs);
        break;
      }
      case TimeCategoryEnum.Day: {
        this.data = this.aggregateByHour(logs);
        break;
      }
    }
  }

  getTimeLogged(log: TimeLog): number {
    const startDate = new Date(log.start);
      const endDate = new Date(log.end);
      return d3.timeMinute.count(startDate, endDate);
  }
  aggregateByMonth(logs: TimeLog[]): Array<AggregationData> {
    const totals = d3.nest<TimeLog, number>().key(d => {
      const startDate = new Date(d.start);
      const roundedMonth = d3.timeMonth(startDate);
      return d3.isoFormat(roundedMonth);
    }).rollup(d => {
      return d3.sum(d, (g: TimeLog) => {
        return this.getTimeLogged(g);
      });
    }).entries(logs);

    return totals.map(entry => {
      return {
        date: d3.isoParse(entry.key),
        timeLogged: entry.value
      };
    });
  }

  aggregateByDay(logs: TimeLog[]): Array<AggregationData> {
    const totals = d3.nest<TimeLog, number>().key(d => {
      const startDate = new Date(d.start);
      const roundedDay = d3.timeDay(startDate);
      return d3.isoFormat(roundedDay);
    }).rollup(d => {
      return d3.sum(d, (g: TimeLog) => {
        return this.getTimeLogged(g);
      });
    }).entries(logs);
    return totals.map(entry => {
      return {
        date: d3.isoParse(entry.key),
        timeLogged: entry.value
      };
    });
  }


  aggregateByHour(logs: TimeLog[]): Array<AggregationData> {
    const totals = d3.nest<TimeLog, number>().key(d => {
      const startDate = new Date(d.start);
      const roundedHour = d3.timeHour(startDate);
      return d3.isoFormat(roundedHour);
    }).rollup(d => {
      return d3.sum(d, (g: TimeLog) => {
        return this.getTimeLogged(g);
      });
    }).entries(logs);


    return totals.map(entry => {
      return {
        date: d3.isoParse(entry.key),
        timeLogged: entry.value
      };
    });
  }


  setTimeCategory(option: string) {
    const timeCategory =  TimeCategoryEnum[option];
    this.dataButtonService.setTimeCategory(timeCategory);
  }

  jumpBack() {
    this.dataButtonService.goBack();
  }

}
