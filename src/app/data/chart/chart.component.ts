import { Component, OnInit, Input, OnChanges, ViewEncapsulation, SimpleChange, SimpleChanges } from '@angular/core';
import * as d3 from 'd3';
import { TimeLog } from '../../Models/LogFormat';
import { AggregationData } from '../log-data/log-data.component';
import * as moment from 'moment';
import { TimeCategoryEnum } from '../../Models/TimeCategoryEnum';

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.less'],
  encapsulation: ViewEncapsulation.None
})
export class ChartComponent implements OnChanges {
  initialized = false;
  @Input() data: Array<AggregationData>;
  @Input() timeCategory: TimeCategoryEnum;
  xAxisFormat: any;
  xScale: any;
  yScale: any;
  xAxis: any;
  yAxis: any;
  xDomain: any;
  chartElement: any;
  margin = {
    top: 50,
    right: 60,
    bottom: 50,
    left: 60
  };
  dimension = {
    width: 900,
    height: 480
  };
    width = this.dimension.width - this.margin.left - this.margin.right;
    height = this.dimension.height - this.margin.top - this.margin.bottom;
    tickPadding = 5;
    axisTitlePadding = Math.max(...Object.values(this.margin)) / 2 + this.tickPadding;

    // colors
    colors: any;
  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.data && this.data !== undefined) {
      this.initChartAttributes();
      if (!this.initialized) {
        this.renderChart();
        this.initialized = true;
      } else { // just need to update
        this.updateAxis();
        this.updateChartData();
      }
    }
  }

  initChartAttributes() {
    this.getXDomain();
    // const dates = this.data.map(res => this.format(res.date));
    this.xScale = d3.scaleBand()
    .domain(this.xDomain)
    .rangeRound([0, this.width]);

    this.xAxis = d3.axisBottom(this.xScale)
    .tickFormat(this.xAxisFormat)
    .tickPadding(this.tickPadding);
    this.scaleYAxis();

    this.colors = d3.scaleLinear().domain([0, this.data.length]).range(<any[]>['lightblue', 'lightgreen']); // d3.schemeGreens[9];

  }
  scaleYAxis() {
    this.yScale = d3.scaleLinear()
    .domain([d3.max(this.data, d => d.timeLogged), 0])
    .range([0, this.height]);

    const max = d3.max(this.data, d => d.timeLogged); // in minutes
    let yAxisFormat = null;
    if (max >= 60) {
      yAxisFormat = function(d) {
          return d3.format('.1f')(d / 60) + 'h';
        };
      } else {
      yAxisFormat = function(d) {return d + 'min'; };
    }
    this.yAxis = d3.axisLeft(this.yScale)
    .tickFormat(yAxisFormat)
    .tickPadding(this.tickPadding);
  }
  getXDomain() {
    const start = d3.min(this.data, d => d.date);
    switch (this.timeCategory) {
      case TimeCategoryEnum.Year: {
        this.xAxisFormat = d3.timeFormat('%B');
        this.xDomain = d3.timeMonth.range(d3.timeYear.floor(start), d3.timeYear.ceil(start));
        break;
      }
      case TimeCategoryEnum.Month: {
        this.xAxisFormat = d3.timeFormat('%d');
        this.xDomain = d3.timeDay.range(d3.timeMonth.floor(start), d3.timeMonth.ceil(start));
        break;
      }
      case TimeCategoryEnum.Day: {
        this.xAxisFormat = d3.timeFormat('%I:%M');
        this.xDomain = d3.timeHour.range(d3.timeDay.floor(start), d3.timeDay.ceil(start));
        break;
      }
      case TimeCategoryEnum.Week: {
        this.xAxisFormat = d3.timeFormat('%A');
        this.xDomain = d3.timeDay.range(d3.timeWeek.floor(start), d3.timeWeek.ceil(start));
        break;
      }
    }
  }
  renderChart() {
    const svg = d3.select('.barSVG').attr('width', this.dimension.width).attr('height', this.dimension.height);
    this.chartElement = svg.append('g').attr('class', 'bar-chart')
    .attr('width', this.width)
    .attr('height', this.height)
    .attr('transform', 'translate(' + this.margin.left + ',' + this.margin.top + ')');
    this.createXAxis();
    this.createYAxis();
    this.createBars();

  }

  createBars() {
    const bar = this.chartElement.selectAll('rect').data(this.data);
    const enterRects = bar.enter().append('rect').transition().duration(1000)
    .attr('width', this.xScale.bandwidth())
    .attr('x', d => this.xScale(d.date))
    .attr('height', (d: any) => this.height - this.yScale(d.timeLogged))
    .attr('y', (d: any) => this.yScale(d.timeLogged))
    .attr('class', 'bar');
    // bar colors and style
    enterRects
    .style('fill', (d, i) => this.colors(i))
    .transition();
  }

  createXAxis() {
    this.chartElement.append('g')
    .attr('class', 'x-axis')
    .attr('transform', 'translate(0,' + this.height + ')')
    .call(this.xAxis);

    // title
    this.chartElement.append('g')
    .attr('class', 'x-axis-title')
    .attr('transform', 'translate(' + (this.width / 2) + ' ,' + (this.height + this.axisTitlePadding) + ')')
    .append('text')
    .style('text-anchor', 'middle');
  }

  createYAxis() {
    this.chartElement.append('g').attr('class', 'y-axis')
    .call(this.yAxis);
    // title
   this.chartElement.append('g')
   .attr('class', 'y-axis-title')
    .append('text')
    .attr('y', -this.axisTitlePadding)
    .attr('x', -this.height / 2)
    .style('text-anchor', 'middle')
    .attr('transform', 'rotate(-90)');
  }
  updateAxis() {
    // update x axis
    this.chartElement.select('g.x-axis')
    .call(this.xAxis);

    // update y axis
    const yAxis = d3.axisLeft(this.yScale).tickPadding(this.tickPadding);
    this.chartElement.select('g.y-axis')
    .call(this.yAxis);
  }

  updateChartData() {
    // console.log(this.data);
    // remove existing bars
    this.chartElement.selectAll('rect').remove().transition();
    this.createBars();

  }

}
