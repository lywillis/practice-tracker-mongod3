import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Subscription, Observable} from 'rxjs';
import {timer} from 'rxjs/observable/timer';
import { TimerService } from 'src/app/services/timer.service';


@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit {
  @Output() timerInfo$: EventEmitter<any> = new EventEmitter<any>();
  @Output() elapsedOutput$: EventEmitter<any> = new EventEmitter<any>();
  startDate: Date;
  endDate: Date;
  // seconds tracker
  ticks = 0;
  start = 0;
  timerStatusSub: Subscription;
  timerSub: Subscription;
  // display clock
  seconds = 0;
  minutes = 0;
  hours = 0;
  startTime: Date;
  constructor(private timerService: TimerService) { }

  ngOnInit() {
    this.timerStatusSub = this.timerService.timerStatus$.subscribe(status => {
      this.handleStatus(status);
    }
    );
  }
  handleStatus(status: any) {
   if (status.play) {
     this.playTimer();
   }
   if (status.pause) {
     this.pauseTimer();
   }
   if (status.stop) {
     this.stopTimer();
   }
  }
  playTimer() {
    this.startDate = new Date();
    this.timerSub = timer(1, 1000).subscribe(ticks => {
      this.ticks = ticks + this.start;
      this.seconds = this.getSeconds(this.ticks);
      this.minutes = this.getMinutes(this.ticks);
      this.hours = this.getHours(this.ticks);
    });
   }

  pauseTimer() {
    this.start += this.ticks;
    this.timerSub.unsubscribe();
   }

  stopTimer() {
    this.endDate = new Date();
    this.elapsedOutput$.emit({
      seconds: this.seconds,
      minutes: this.minutes,
      hours: this.hours
    });
    // clear timer
    this.start = 0;
    this.ticks = 0;
    this.seconds = 0;
    this.minutes = 0;
    this.hours = 0;
    this.timerSub.unsubscribe();
    this.timerInfo$.emit({start: this.startDate, end: this.endDate});
   }

   private getSeconds(ticks: number) {
     return ticks % 60;
   }
   private getMinutes(ticks: number) {
     return Math.floor(ticks / 60) % 60;
   }
   private getHours(ticks: number) {
     return Math.floor((ticks / 60) / 60);
   }


}
