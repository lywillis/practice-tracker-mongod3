export enum TimeCategoryEnum {
    Day,
    Month,
    Week,
    Year
}
