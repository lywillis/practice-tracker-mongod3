export interface TimeLog { // comes from mongoDB
    _id?: string;
    start: string;
    end: string;
    task: string;
}


export interface Task {
    _id?: string;
    name: string;
}

