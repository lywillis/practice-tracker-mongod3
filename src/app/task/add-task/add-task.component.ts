import { Component, OnInit } from '@angular/core';
import { Task } from '../../Models/LogFormat';
import { MongoDBService } from '../../services/mongoDB.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.less']
})
export class AddTaskComponent implements OnInit {
  constructor(private mongoDBService: MongoDBService, public activeModal: NgbActiveModal) { }
  newTask = '';
  ngOnInit() {
  }
  saveTask(form: any) {
    const newTask: Task = {name: this.newTask};
    this.mongoDBService.addTask(newTask);
    this.newTask = '';
    this.activeModal.close();
  }

}
