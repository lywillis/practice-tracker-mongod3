import { Component, OnInit } from '@angular/core';
import { MongoDBService } from '../../services/mongoDB.service';
import { Task } from '../../Models/LogFormat';
import { Subscription } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddTaskComponent } from '../add-task/add-task.component';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.less']
})
export class TaskListComponent implements OnInit {
  tasks: Array<Task>;
  mongoSub: Subscription;
  constructor(private mongoDBService: MongoDBService, private modalService: NgbModal) { }

  ngOnInit() {
    this.getTaskList();
    this.mongoSub = this.mongoDBService.taskAddSubject.subscribe(response => {
      this.getTaskList();
    });
  }
  getTaskList() {
    this.mongoDBService.getTasks().then(
      res => this.tasks = res);
  }

  openAddTask() {
    this.modalService.open(AddTaskComponent, {centered: false, size: 'sm'});
  }

}
