import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { MongoDBService } from './services/mongoDB.service';
import { HttpModule } from '@angular/http';
import { LogListComponent } from './log/log-list/log-list.component';
import { ChartComponent } from './data/chart/chart.component';
import { LogDataComponent } from './data/log-data/log-data.component';
import { TaskSelectComponent } from './data/task-select/task-select.component';
import {FormsModule} from '@angular/forms';
import { TimerButtonsComponent } from './timer/timer-buttons/timer-buttons.component';
import { TimerComponent } from './timer/timer/timer.component';
import { AddTaskComponent } from './task/add-task/add-task.component';
import { TaskListComponent } from './task/task-list/task-list.component';
import { AddLogComponent } from './log/add-log/add-log.component';
import { DatepickerComponent } from './data/datepicker/datepicker.component';

@NgModule({
  declarations: [
    AppComponent,
    LogListComponent,
    ChartComponent,
    LogDataComponent,
    TaskSelectComponent,
    TimerButtonsComponent,
    TimerComponent,
    AddTaskComponent,
    TaskListComponent ,
    AddLogComponent,
    DatepickerComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    HttpModule,
    FormsModule
  ],
  providers: [MongoDBService],
  bootstrap: [AppComponent],
  entryComponents: [AddLogComponent, AddTaskComponent]
})
export class AppModule { }
